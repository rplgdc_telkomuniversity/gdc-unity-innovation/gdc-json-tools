# GDC Json-Tools
## Built In
![Build Status](https://badgen.net/badge/Build/Passing/green) ![Version](https://badgen.net/badge/Release/v1.1.0/blue) ![Languages](https://badgen.net/badge/Languages/C-Sharp/blue)
![Engine](https://badgen.net/badge/Engine/Unity%202020.3.4f1/gray)

Depedencies
- com.unity.nuget.newtonsoft-json : 2.0.0

## Installation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## What is it?
GDC Json-Tools is a package that makes developer easier to work with JSON.
without further do, lets go to implementation!

## New feature! v1.1.0
- Saving object directly to file!
- Load object directly from file!
- Encrypt your data


## Getting Started
##### Convert Object to JSON & Saving
To convert object to JSON string, use code snippet bellow
```cs
using GDC.Innovation.Json;
public class NewScript
{
    public string thisIsString = "Try";
    public float thisIsFloat = 5;
}

public class Main : MonoBehaviour
{
    private void Start()
    {
        NewScript someObject = new NewScript();
        string json = GDCUtils.ConvertObjectToJsonString<NewScript>(someObject, "som3Passw0rd");
    
        GDCUtils.SaveFileToLocation(json, "somepath/somefile.json");
    }
}
```
Version v1.1.0
```cs
using GDC.Innovation.Json;
public class NewScript
{
    public string thisIsString = "Try";
    public float thisIsFloat = 5;
}

public class Main : MonoBehaviour
{
    private void Start()
    {
        NewScript someObject = new NewScript();

        bool issuccess = GDCUtils.SaveObjectToFile<NewScript>("somepath/somefile.json" , someObject, "som3Passw0rd");
    }
}
```


##### Load & Convert JSON to Object
To convert object to JSON string, use code snippet bellow
```cs
using GDC.Innovation.Json;
public class NewScript
{
    public string thisIsString = "Try";
    public float thisIsFloat = 5;
}

public class Main : MonoBehaviour
{
    private void Start()
    {
        NewScript someObject = null;

        string json = GDCUtils.LoadFileFromLocation("somepath/somefile.json");
        if (json != null)
        {
            someObject = GDCUtils.ConvertStringJsonToObject<NewScript>(json, "som3Passw0rd");
        }
        else 
        {
            Debug.LogError("Can't find files");
        }
    }
}
```

Version v1.1.0
```cs
using GDC.Innovation.Json;
public class NewScript
{
    public string thisIsString = "Try";
    public float thisIsFloat = 5;
}

public class Main : MonoBehaviour
{
    private void Start()
    {
        NewScript someObject = null;

        someObject = GDCUtils.LoadObjectFromFile<NewScript>("somepath/somefile.json" , "som3Passw0rd");
    }
}
```

## Contributor


| Profile | 
| ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | 

## License

MIT

**Free Software, Hell Yeah!**

