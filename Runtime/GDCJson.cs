﻿using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using GDC.Innovation.Json.Editor;

namespace GDC.Innovation.Json
{

    public class GDCJson
    {
        /// <summary>
        /// Instantly Load object from file location
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="location"></param>
        /// <returns></returns>
        public static T LoadObjectFromFile<T>(string location , string decryptPassword = null)
        {
            string json = LoadFileFromLocation(location);
            if (!string.IsNullOrEmpty(json))
            {
                return ConvertStringJsonToObject<T>(json , decryptPassword);
            }
            else
            {
                Debug.LogWarning("Can't find file in location!");
                return default(T);
            }
        }

        /// <summary>
        /// Loading string from a file in local storage user
        /// </summary>
        /// <param name="location">location of file. ex: gameplay/user-setting.json</param>
        /// <returns>json as string</returns>
        public static string LoadFileFromLocation(string location)
        {
            string path = Path.Combine(Application.persistentDataPath, location);
            return LoadFileFromPath(path);
        }

        static string LoadFileFromPath(string path)
        {
            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }
            return null;
        }

        /// <summary>
        /// Instantly Save Object to file in specific location
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="location"></param>
        /// <param name="someObject"></param>
        /// <returns></returns>
        public static bool SaveObjectToFile<T>(string location, T someObject, string encryptPassword = null)
        {
            string json = ConvertObjectToJsonString<T>(someObject , encryptPassword);
            if (string.IsNullOrEmpty(json))
            {
                return false;
            }
            else
            {
                SaveFileToLocation(json, location);
                return true;
            }
        }

        /// <summary>
        /// Save json string from a string to local storage user
        /// </summary>
        /// <param name="location">specific location of file will be saved. ex: gameplay/user-setting.json</param>
        /// <param name="value">json string</param>
        public static void SaveFileToLocation(string location, string value)
        {
            string path = Path.Combine(Application.persistentDataPath, location);
            SaveFileToPath(path, value);
        }
        static void SaveFileToPath(string path, string value)
        {
            File.WriteAllText(path, value);
        }

        /// <summary>
        /// Convert object to json string
        /// </summary>
        /// <typeparam name="T">Data Type</typeparam>
        /// <param name="objek">object</param>
        /// <returns></returns>
        public static string ConvertObjectToJsonString<T>(T objek, string encryptPassword = null)
        {
            string json = JsonConvert.SerializeObject(objek);
            
            //encrypt object
            if (!string.IsNullOrEmpty(json) && !string.IsNullOrEmpty(encryptPassword))
            {
                json = StringCipher.Encrypt(json, encryptPassword);
            }

            return json;
        }

        /// <summary>
        /// Convert json string to object
        /// </summary>
        /// <typeparam name="T">data type</typeparam>
        /// <param name="json">json string</param>
        /// <returns>T object</returns>
        public static T ConvertStringJsonToObject<T>(string json, string encryptPassword = null)
        {
            //decrypt object
            if (!string.IsNullOrEmpty(encryptPassword))
            {
                json = StringCipher.Decrypt(json, encryptPassword);
            }

            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Delete file in directory (if it exist)
        /// </summary>
        /// <param name="location">specific location of files</param>
        /// <returns>boolean, is success deleting file?</returns>
        public static bool DeleteFile(string location)
        {

            string filePath = Path.Combine(Application.persistentDataPath, location);

            // check if file exists
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            return false;
        }
    }
}
